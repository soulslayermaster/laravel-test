Proposal for next issues:
Poor image handling (low-resolution photos)
Broken images

In order to fix this we need to change the image processing a bit. 
Project already have the intervention/image package that based on imagik-library.

To fix the image handling we need to change the upload logik and save the image in 3 variants.
(a thumbnail to show on page, a priew (basically a bigger version of thumbnail), and full image ( that is usually only available for download).
The imagik allows that. Also it needs to be paired with good js image crop-library in admin panel for editing what part of image to show, not to "spoil" image
when it is shown on page.

This can also speed the pageload up beause we will show the processed image. (Also for this issue it may be a good idea to convert images to a different format (png to img) for example, but this should be threated very carefully not to spoil the image. 

For image reordering we need a good file-manager plugin like for example unisharp/laravel-filemanager (it is also can be pared with intervention/image to add direct editing of images to admin panel). This plugin will also allow a full file operation with the image. 
